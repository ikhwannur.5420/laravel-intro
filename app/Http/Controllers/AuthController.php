<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('form');
    }

    public function welcome(Request $req)
    {
        $nama = $req['firstName'];
        return view('hello', ['nama' => $nama]);
    }
}
