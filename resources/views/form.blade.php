<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
        @csrf
        <p>First name:</p>
        <input type="text" name="firstName" id="firstName">

        <p>Last name:</p>
        <input type="text" name="lastName" id="lastName">

        <p>Gender:</p>
        <input type="radio" name="gender" id="gender" value="Male">
        <label for="Male">Male</label><br>
        <input type="radio" name="gender" id="gender" value="Female">
        <label for="Female">Female</label><br>
        <input type="radio" name="gender" id="gender" value="Other">
        <label for="Other">Other</label><br>

        <p>Nationality:</p>
        <select name="nationality" id="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporeian">Singaporeian</option>
            <option value="Malaysian">Malaysian</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" id="language1" name="language1" value="Bahasa Indonesia">
        <label for="language1">Bahasa Indonesia</label><br>
        <input type="checkbox" id="language2" name="language2" value="English">
        <label for="language2">English</label><br>
        <input type="checkbox" id="language3" name="language3" value="Other">
        <label for="language3">Other</label><br>

        <p>Bio:</p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>

        <input type="submit" value="Sign Up">
    </form>
</body>

</html>